/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ACTSTRK_STRIPCLUSTERCACHEID_H
#define ACTSTRK_STRIPCLUSTERCACHEID_H

#include "xAODInDetMeasurement/StripCluster.h"
#include "src/Cache.h"

CLASS_DEF(ActsTrk::Cache::Handles<xAOD::StripCluster>::IDCBackend, 202232989, 1);
#endif