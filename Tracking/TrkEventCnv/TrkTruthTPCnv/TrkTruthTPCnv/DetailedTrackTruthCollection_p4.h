/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

// Dear emacs, this is -*-c++-*-

// Andrei.Gaponenko@cern.ch, 2008
// Olivier.Arnaez@cern.ch, 2015

#ifndef TRKTRUTHTPCNV_DETAILEDTRACKTRUTHCOLLECTION_P4_H
#define TRKTRUTHTPCNV_DETAILEDTRACKTRUTHCOLLECTION_P4_H

#include "TrkTruthTPCnv/TrackTruthKey_p0.h"
#include "TrkTruthTPCnv/DetailedTrackTruth_p4.h"
#include "DataModelAthenaPool/DataLink_p1.h"
#include "TrkTrack/TrackCollection.h"

#include "AthenaKernel/CLASS_DEF.h"

#include <vector>

namespace Trk {

  class DetailedTrackTruthCollection_p4 {
  public:

    DataLink_p1 m_trackCollectionLink;

    // Perhaps can use here a 32 bit unsigned instead of the 64 bit one?
    typedef TrackCollection::size_type size_type;

    struct Entry {
      TrackTruthKey_p0 key;
      DetailedTrackTruth_p4 detailedTruth;
    };

    typedef std::vector<Entry> CollectionType;
    CollectionType m_entries;
  };

}

CLASS_DEF( Trk::DetailedTrackTruthCollection_p4 , 1321426764 , 1 )

#endif/*TRKTRUTHTPCNV_DETAILEDTRACKTRUTHCOLLECTION_P4_H*/
